#include <stdio.h>
#include "../alsafs_fs.h"

int main()
{
    fd = open("fs.alsafs", O_RDWR);
    fs_init();
    mode_t mode = 0;

    char buff[8];

    do_mkdir("dir1", mode);

    for (int i = 0; i <= 8; i++)
    {
        sprintf(buff, "F %d", i);
        do_create(buff, mode, NULL);
    }

}
