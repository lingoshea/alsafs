#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "block_sec.h"
#include "alsafs_fs.h"

#define BLOCK_SIZE 32

#define ASSERT(action) if (!(action)) { return -1; }

// FILE/DIR node :
// (1 byte flag / 32bit attr (4 byte) /
// 1 byte filename len / 128 byte name /
// 64bit index count / * 64bit indexes to nodes)

// in fsroot, [1 byte filename len / 128 byte name] is put in other use

int format()
{
    off_t file_size = lseek(fd, 0L, SEEK_END);
    lseek(fd, 0L, SEEK_SET);
    bk_count = (file_size - sizeof(bk_count_t) - sizeof(bk_size_t)) / BLOCK_SIZE;
    bk_size  = BLOCK_SIZE;
    node_flag_t flag = SUPER_BLOCK_INDEX;
    index_count_t indexCount = 0;
    attr_t attr = 0xff;

    // fs head
    ASSERT ( WRITE_VERIFY(bk_count) )
    ASSERT ( WRITE_VERIFY(bk_size) )

    ASSERT ( goto_bk_by_off(0) )
    ASSERT ( WRITE_VERIFY(flag) )
    ASSERT ( WRITE_VERIFY(indexCount) )

    ASSERT ( block_registry(1) )
    ASSERT ( goto_bk_by_off(1) )
    flag = DIR_BLOCK | FSROOT_BLOCK;
    ASSERT ( WRITE_VERIFY(flag))
    ASSERT ( WRITE_VERIFY(attr))
    ASSERT ( lseek(fd, MAX_NAME_LEN + sizeof(name_len_t), SEEK_CUR) != -1)
    ASSERT ( WRITE_VERIFY(indexCount))

    flag = NOT_USED;

    for (bk_index_t i = 1; i < bk_count; i++)
    {
        ASSERT ( goto_bk_by_off(i) )
        ASSERT ( WRITE_VERIFY(flag) )
    }

    return 0;
}

int main(int argc, char ** argv)
{
    // FIXME: !!! arguments sucked !!!
    if (argc > 1)
    {
        fd = open(argv[1], O_RDWR);

        if (fd < 0)
        {
            return -1;
        }

        int result = format();
        return result && close(fd);
    }
}
