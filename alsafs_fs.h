#ifndef _ALSAFS_FS_H_
#define _ALSAFS_FS_H_

#define FUSE_USE_VERSION 30
#include <sys/types.h>
#include <fuse.h>
#include <inttypes.h>

typedef uint64_t bk_count_t;        // 64bit block numbers
typedef uint32_t attr_t;            // 32bit attr
typedef uint32_t bk_size_t;         // 32bit block size
typedef uint64_t bk_index_t;        // 64bit block index
typedef uint64_t data_size_t;       // 64bit data size
typedef uint8_t  node_flag_t;       // 8bit  node flag
typedef uint64_t index_count_t;     // 64bit index count
typedef uint64_t fs_size_t;         // 64bit size
typedef uint8_t  name_len_t;        // 8bit  length of name

/*                                                                 8 4 2 1 | 8 4 2 1 */
#define SUPER_BLOCK_INDEX ((node_flag_t)0x00) /* super block index 0 0 0 0 | 0 0 0 0  1 */
#define FILE_BLOCK        ((node_flag_t)0x01) /* file block        0 0 0 0 | 0 0 0 *  2 */
#define DATA_BLOCK        ((node_flag_t)0x02) /* data block        0 0 0 0 | 0 0 * 0  3 */
#define BLOCK_LINK        ((node_flag_t)0x80) /* node link         * 0 0 0 | 0 0 0 0  4 >>COB */
#define DIR_BLOCK         ((node_flag_t)0x04) /* directory block   0 0 0 0 | 0 * 0 0  5 */
#define NOT_USED          ((node_flag_t)0x08) /* unused block      0 0 0 0 | * 0 0 0  6 */
#define FSROOT_BLOCK      ((node_flag_t)0x10)   /* filesystem root 0 0 0 * | 0 0 0 0  7 >>COB */
// Note that FSROOT is not created on mount, and not deletable
// Also in FSROOT name is not used. But it will remain untouched, and followed by index

#define FSROOT_BLOCK_LOCATION ((bk_index_t)1) /* location of filesystem root */
#define FSINIT_SUPBK_LOCATION ((bk_index_t)0) /* first super block location */

int do_read         ( const char *, char *, size_t, off_t, struct fuse_file_info *);
int do_readdir      ( const char *, void *, fuse_fill_dir_t, off_t, struct fuse_file_info *);
int do_getattr      ( const char *, struct stat *);
int do_mkdir        (const char *, mode_t);
int do_rmdir        (const char *);
int do_open         (const char *, struct fuse_file_info *);
int do_write        (const char *, const char *, size_t, off_t, struct fuse_file_info *);
int do_create       (const char *, mode_t, struct fuse_file_info *);
int do_unlink       (const char *);
int do_truncate     (const char *, off_t);

extern bk_count_t bk_count;
extern bk_size_t  bk_size;
extern int        fd;
extern struct fuse_operations operations;

int fs_init();
unsigned long resolve(const char * pathname, char * buff, int * end);

#define MAX_NAME_LEN ((name_len_t)8)

#define MIN(a,b) ((a) < (b) ? (a) : (b))

#endif //_ALSAFS_FS_H_
