cmake_minimum_required(VERSION 3.10)

project(alsafs)

find_package(PkgConfig)
pkg_check_modules(FOUND_FUSE fuse)

include_directories(include ${FOUND_FUSE})

set (CMAKE_C_FLAGS "-fprofile-arcs -fbranch-probabilities -ftest-coverage --coverage -fPIC --pie -D_FILE_OFFSET_BITS=64")
set (CMAKE_C_STANDARD 11)

set (SOURCE_FILES
        alsafs_fs.h block_sec.h block_sec.c file.c dir.c ops.c genetic.c
        )

set (ALSAFS_UNIT_TESTS
        do_create
        )

add_executable(mount.alsafs  ${SOURCE_FILES} fuse_main.c alsafs.c)
target_link_libraries(mount.alsafs fuse pthread)

add_executable(mkfs.alsafs   mkfs.alsafs.c ${SOURCE_FILES})

if ( ${CMAKE_BUILD_TYPE} STREQUAL "Debug" )

    enable_testing ()
    message("Unit Tests Enabled")

    FOREACH( ALSAFS_UNIT_TEST_SIG ${ALSAFS_UNIT_TESTS} )
        add_executable(${ALSAFS_UNIT_TEST_SIG}
                test/${ALSAFS_UNIT_TEST_SIG}.c ${SOURCE_FILES})

        add_test( NAME ${ALSAFS_UNIT_TEST_SIG}
                COMMAND ${ALSAFS_UNIT_TEST_SIG} )

        message("Unit test \"" "${ALSAFS_UNIT_TEST_SIG}" "\" Enabled")
    ENDFOREACH( ALSAFS_UNIT_TEST_SIG )

endif()
