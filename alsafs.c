#include <unistd.h>
#include <string.h>

#include "alsafs_fs.h"
#include "block_sec.h"

struct fuse_operations operations = {
    .create     = do_create,
    .mkdir      = do_mkdir,
};
