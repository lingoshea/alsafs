#include <fuse.h>
#include <unistd.h>

#include "alsafs_fs.h"
#include "block_sec.h"

int main(int argc, char ** argv)
{
    fd = open(argv[argc], O_RDWR);
    ON_ERR_GO_ERR(fs_init(), error);
    int result = fuse_main(argc - 1, argv, &operations, NULL);
    return result && close(fd);

error:
    close(fd);
    return -1;
}


