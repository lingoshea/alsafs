#include <zconf.h>
#include <stdio.h>
#include <asm/errno.h>
#include <string.h>
#include <errno.h>
#include <malloc.h>

#include "alsafs_fs.h"
#include "block_sec.h"


// FILE/DIR node :
// (1 byte flag / 32bit attr (4 byte) /
// 1 byte filename len / 128 byte name /
// 64bit index count / * 64bit indexes to nodes)
/* genetic make super block func */
int make (const char * pathname, node_flag_t flag)
{
    static index_count_t index_count = 0;
    static attr_t        attr        = 0xff;
    static char          name_buff [MAX_NAME_LEN + 1];

    bk_index_t upper_dir = fs_hook(pathname, name_buff);

    // dir block is a super block, registration required
    bk_index_t dir_block = next_avil_bk();
    ON_ERR_GO_ERR(dir_block != -1, error) // check block
    ON_ERR_GO_ERR(block_registry(dir_block) != -1, error) // registry
    ON_ERR_GO_ERR(goto_bk_by_off(dir_block) != -1, error) // goto block location
    ON_ERR_GO_ERR(WRITE_VERIFY(flag), error) // write flag
    ON_ERR_GO_ERR(WRITE_VERIFY(attr), error) // write attr
    name_len_t write_len = MIN(strlen(name_buff), MAX_NAME_LEN); // get the actual name len for write
    ON_ERR_GO_ERR(WRITE_VERIFY(write_len), error) // write 1 byte name len
    ON_ERR_GO_ERR(write(fd, name_buff, write_len) == write_len, error) // write name
    ON_ERR_GO_ERR(lseek(fd, MAX_NAME_LEN - write_len, SEEK_CUR) != -1, error) // seek to end of the name filed
    ON_ERR_GO_ERR(WRITE_VERIFY(index_count), error) // write index count, which is 0

    return 0;

error:
    return -ENOMEM; // no fucking space
}
