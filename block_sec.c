#include <unistd.h>
#include <errno.h>
#include <string.h>

#include "block_sec.h"

#define READ_A_NUMBER(num_type, fd) \
{ \
    num_type num_to_read; \
    if ( sizeof(num_to_read) == read(fd, &num_to_read, sizeof (num_to_read)) ) \
    { \
        return num_to_read; \
    } \
    return 0; \
}

bk_count_t read_bk_count()
{
    READ_A_NUMBER(bk_count_t, fd)
}

bk_size_t read_bk_size()
{
    READ_A_NUMBER(bk_size_t, fd);
}

off_t goto_bk_by_off(bk_index_t index)
{
    if (index > bk_count)
    {
        return -1;
    }

    off_t offset = bk_size * index + sizeof(bk_count) + sizeof(bk_size);
    return lseek(fd, offset, SEEK_SET);
}


// node index ext ( 1 byte falg / 64bit index count / * 64bit index )

bk_index_t index_block_read(bk_index_t block, index_count_t which_one)
{
    ON_ERR_GO_ERR(goto_bk_by_off(block) != -1, error_1)

    node_flag_t     flag;
    index_count_t   index_num;
    bk_index_t      index;

    ON_ERR_GO_ERR(READ_VERIFY(flag),      error_1) // 1 byte flag
    ON_ERR_GO_ERR(READ_VERIFY(index_num), error_1) // 64bit index count

    if (which_one > index_num)
    {
        return 0;
    }

    off_t offset = which_one * sizeof(bk_index_t);

    ON_ERR_GO_ERR(lseek(fd, offset, SEEK_CUR) != -1, error_1)
    ON_ERR_GO_ERR(READ_VERIFY(index), error_1)

    return index;

error_1:
    return 0;
}

int index_block_write(bk_index_t block, index_count_t which_one, bk_index_t index_for_wr)
{
    ON_ERR_GO_ERR(goto_bk_by_off(block) != -1, error_1)

    node_flag_t     flag;
    index_count_t   index_num;
    index_count_t   max_index_num = (bk_size - sizeof(node_flag_t) - sizeof(index_count_t)) / sizeof(bk_index_t);

    ON_ERR_GO_ERR(READ_VERIFY(flag),      error_1)
    ON_ERR_GO_ERR(READ_VERIFY(index_num), error_1)

    if (which_one > max_index_num)
    {
        return -1;
    }

    off_t offset = which_one * sizeof(bk_index_t);

    ON_ERR_GO_ERR(lseek(fd, offset, SEEK_CUR) != -1, error_1)
    ON_ERR_GO_ERR(WRITE_VERIFY(index_for_wr), error_1)

    return 0;

error_1:
    return -1;
}

// node index ( 1 byte falg / 64bit index count / * 64bit index ) same as node index ext

int block_registry(bk_index_t index)
{
    node_flag_t    flag;
    index_count_t  cur_index_count;
    bk_index_t     super_bk_index = 0;
    index_count_t  max_index_num = (bk_size - sizeof(node_flag_t) - sizeof(index_count_t)) / sizeof(bk_index_t);

lookup:

    ON_ERR_GO_ERR(goto_bk_by_off(super_bk_index) != -1, error)
    ON_ERR_GO_ERR(READ_VERIFY(flag), error) // 1 byte flag
    ON_ERR_GO_ERR(READ_VERIFY(cur_index_count), error) // 64bit index count

    if (flag & BLOCK_LINK)
    {
        super_bk_index = index_block_read(super_bk_index, cur_index_count);
        goto lookup;
    }

    if (cur_index_count == max_index_num) // reached but not exceed
    {
        bk_index_t index_for_wr = index_block_read(super_bk_index, cur_index_count);
        ON_ERR_GO_ERR(index_for_wr != -1, error)
        ON_ERR_GO_ERR(goto_bk_by_off(super_bk_index) != -1, error)
        flag |= BLOCK_LINK;
        ON_ERR_GO_ERR(WRITE_VERIFY(flag), error)
        super_bk_index = next_avil_bk();
        // found one
        ON_ERR_GO_ERR(index_block_write(super_bk_index, 0, index_for_wr) != -1, error);
        ON_ERR_GO_ERR(index_block_write(super_bk_index, 1, index) != -1, error);
    }
    else
    {
        index_block_write(super_bk_index, cur_index_count, index);
    }

    return 0;

error:
    return -1;
}

bk_index_t next_avil_bk()
{
    bk_index_t super_bk_index = 0;
    node_flag_t node;
    do
    {
        super_bk_index++; // starts with 1
        ON_ERR_GO_ERR(goto_bk_by_off(super_bk_index) != -1, error)
        ON_ERR_GO_ERR(READ_VERIFY(node), error)
    } while (!(node & NOT_USED));

    // found one
    return super_bk_index;

error:
    return 0;
}

bk_index_t lookup_in_dir(const char * pathname, node_flag_t flag, bk_index_t init_dir)
{
    if (init_dir == FSINIT_SUPBK_LOCATION) // dispatch this result
    {
        return 0;
    }

    // now the real shit starts
    index_count_t index_count;
    bk_index_t    block_index;
    node_flag_t   node_flag, init_flag;
    off_t         cur_offset;
    name_len_t    name_len;
    char buff[MAX_NAME_LEN + 1];

    ON_ERR_GO_ERR(goto_bk_by_off(init_dir) != -1, error) // goto fsroot
    ON_ERR_GO_ERR(READ_VERIFY(init_flag), error)  // read the init node
    // FIXME: YOU SHOULD CHECK IT INSTEAD OF SKIPPING !!!
    off_t skipped_offset = sizeof(attr_t) + MAX_NAME_LEN + sizeof(name_len_t);
    ON_ERR_GO_ERR(lseek(fd,
        skipped_offset,
        SEEK_CUR) != -1, error)

    ON_ERR_GO_ERR(READ_VERIFY(index_count), error)
    index_count_t max_index_num = index_count;
    index_count_t wh_thefuck_iam = 0; // read but hasn't jump to

    // save current offset for resume
    cur_offset = lseek(fd, 0, SEEK_CUR);

lookup:
    ON_ERR_GO_ERR(READ_VERIFY(block_index), error)
    cur_offset += sizeof(bk_index_t);

    if (wh_thefuck_iam == max_index_num) // about to check the last one
    {
        if (init_flag & BLOCK_LINK) // if it's a b-link
        {
            // then we must fucking jump to a new node
            ON_ERR_GO_ERR(goto_bk_by_off(block_index) != -1, error)
            ON_ERR_GO_ERR(READ_VERIFY(init_flag), error) // flag, should say it's an index
            ON_ERR_GO_ERR(READ_VERIFY(index_count), error) // index count
            cur_offset = lseek(fd, 0, SEEK_CUR);
            wh_thefuck_iam = 0;
            // structure changed here:
            max_index_num = index_count;
            goto lookup;
        }
    }

    ON_ERR_GO_ERR(goto_bk_by_off(block_index) != -1, error)
    ON_ERR_GO_ERR(READ_VERIFY(node_flag), error)
    if (node_flag & flag)
    {
        // FIXME: YOU SHOULD CHECK IT INSTEAD OF SKIPPING !!!
        ON_ERR_GO_ERR(lseek(fd, sizeof(attr_t), SEEK_CUR) != -1, error)
        ON_ERR_GO_ERR(READ_VERIFY(name_len), error)
        ON_ERR_GO_ERR(name_len == read(fd, buff, name_len), error)
        buff [name_len] = 0;
        if (!strcmp(buff, pathname))
        {
            return block_index;
        }
    }

    // if you made this far, then you didn't find it
    if (wh_thefuck_iam < max_index_num)
    {
        ON_ERR_GO_ERR(lseek(fd, cur_offset, SEEK_SET) != -1, error)
        wh_thefuck_iam++;
        goto lookup;
    }

error:
    return 0; // didn't find it
}

bk_index_t fs_hook(const char * pathname, char * external_name_buff)
{
    char name_buff[MAX_NAME_LEN + 1];
    unsigned long name_len;
    bk_index_t init_dir = FSROOT_BLOCK_LOCATION;
    int end = 0;

    do
    {
        name_len = resolve(pathname, name_buff, &end);

        if (end)
        {
            break;
        }

        name_buff[name_len] = 0;

        init_dir = lookup_in_dir(name_buff, DIR_BLOCK, init_dir);

    } while ( init_dir );

    memcpy(external_name_buff, name_buff, strlen(name_buff));
    return init_dir;
}

bk_index_t fs_lookup(const char * pathname, node_flag_t flag, char * external_name_buff)
{
    bk_index_t init_dir = fs_hook(pathname, external_name_buff);
    return lookup_in_dir(external_name_buff, flag, init_dir);
}


