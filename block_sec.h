#ifndef _BLOCK_SEC_H_
#define _BLOCK_SEC_H_

#include "alsafs_fs.h"

// First to call
/* Read filesystem block numbers(count)
 * return 0 on error, errno set
 * return number otherwise */
bk_count_t read_bk_count();

// Second to call
/* Read filesystem block size
 * return 0 on error, errno set
 * return number otherwise */
bk_size_t read_bk_size();

/* get offset of a block
 * return -1 on error, errno set
 * return number otherwise */
off_t goto_bk_by_off(bk_index_t index);

/* create a data block at block ${index}
 * return *-1* on error, errno set
 * return number otherwise */
int create_data_block(bk_index_t index);

/* read index block, return 0 on error */
bk_index_t index_block_read(bk_index_t block, index_count_t which_one);

/* create a super block index for look up
 * return *-1* on error, errno set
 * return number otherwise */
int block_registry(bk_index_t index);

/* find next available block, return 0 on error */
bk_index_t next_avil_bk();

int index_block_write(bk_index_t block, index_count_t which_one, bk_index_t index_for_wr);
bk_index_t lookup_in_dir(const char * pathname, node_flag_t flag, bk_index_t init_dir);
bk_index_t fs_hook(const char * pathname, char * external_name_buff);
bk_index_t fs_lookup(const char * pathname, node_flag_t flag, char * external_name_buff);


int make (const char * pathname, node_flag_t flag);

#define ON_ERR_GO_ERR(action, flag) if (!(action)) { goto flag; }
#define READ_VERIFY(num)  (read(fd, &num, sizeof(num)) == sizeof(num))
#define WRITE_VERIFY(num) (write(fd, &num, sizeof(num)) == sizeof(num))

#endif //_BLOCK_SEC_H_
