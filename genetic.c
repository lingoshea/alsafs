#include <unistd.h>
#include <string.h>

#include "alsafs_fs.h"
#include "block_sec.h"

bk_count_t bk_count;
bk_size_t  bk_size;
int        fd;

int fs_init()
{
    bk_count = read_bk_count();
    bk_size  = read_bk_size();

    return (bk_count && bk_size);
}

unsigned long resolve(const char * pathname, char * buff, int * end)
{
    // /dir/files/dir/files/
    // /dir/files
    char * slash_bfr = strchr(pathname, '/');
    char * slash_aft = strchr(slash_bfr + 1, '/'); // it's save

    if (!slash_bfr)
    {
        return 0;
    }

    if (!slash_aft)
    {
        strcpy(buff, slash_bfr + 1);
        *end = 1;
        return 0;
    }

    long sz = slash_aft - slash_bfr - 1;

    memcpy(buff, slash_bfr + 1, sz);

    // /dir/files/dir/files/
    //                     ^
    if (*(slash_aft + 1) == 0)
    {
        *end = 1;
    }

    return sz;
}


